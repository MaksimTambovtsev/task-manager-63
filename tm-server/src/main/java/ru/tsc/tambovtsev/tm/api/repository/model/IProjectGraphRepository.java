package ru.tsc.tambovtsev.tm.api.repository.model;

import ru.tsc.tambovtsev.tm.model.Project;

public interface IProjectGraphRepository extends IOwnerGraphRepository<Project> {
}