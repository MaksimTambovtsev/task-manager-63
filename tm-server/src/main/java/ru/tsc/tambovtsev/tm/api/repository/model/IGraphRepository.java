package ru.tsc.tambovtsev.tm.api.repository.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

@NoRepositoryBean
public interface IGraphRepository<M extends AbstractEntity> extends JpaRepository<M, String> {
}
