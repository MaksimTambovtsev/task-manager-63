package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractEntityDTO> {

    @Nullable
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    boolean existsById(@Nullable String id);

    @Nullable
    M findById(@Nullable String id);

    @Nullable
    int getSize();

    @Nullable
    M remove(@Nullable M model);

    @Nullable
    M removeById(@Nullable String id);

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    @Nullable
    List<M> findAll(@Nullable SortTable sortTable);

    void removeAll(@Nullable Collection<M> collection);

}
