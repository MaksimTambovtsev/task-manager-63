<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>PROJECT EDIT</h1>

<form action="/project/edit?id=${project.id}" method="post">

    <input type="hidden" name="id" value="${project.id}" />

    <p>
        <div>Name:</div>
        <div>
            <input type="text" name="name" value="${project.name}" />
        </div>
    </p>

    <p>
        <div>Description:</div>
        <div>
            <input type="text" name="description" value="${project.description}" />
        </div>
    </p>

    <p>
        <div>Status:</div>
        <select name="status">
            <c:forEach var="status" items="${statuses}">
                <option <c:if test="${project.status == status}">selected="selected"</c:if> value="${status}">
                    ${status.displayName}
                </option>
            </c:forEach>
        </select>
    </p>

    <p>
        <div>Date start:</div>
        <div>
            <input type="date" name="dateBegin"
                   value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateBegin}" />" />
        </div>
    </p>

    <p>
        <div>Date End:</div>
        <div>
            <input type="date" name="dateEnd" value="<fmt:formatDate pattern="yyyy-MM-dd" value="${project.dateEnd}" />" />
        </div>
    </p>

    <button type="submit">Save project</button>
</form>

<jsp:include page="../include/_footer.jsp" />