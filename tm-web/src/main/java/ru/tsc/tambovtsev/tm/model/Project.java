package ru.tsc.tambovtsev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name ="";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date dateBegin;

    @NotNull
    private Date dateEnd;

    public Project(@NotNull String name) {
        this.name = name;
    }

}
