package ru.tsc.tambovtsev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        write(new Project("Project 1"));
        write(new Project("Project 2"));
        write(new Project("Project 3"));
    }

    public void write(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        write(new Project("New Project" + System.currentTimeMillis()));
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
