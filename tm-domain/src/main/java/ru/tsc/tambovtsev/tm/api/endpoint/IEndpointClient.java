package ru.tsc.tambovtsev.tm.api.endpoint;

import java.io.IOException;

public interface IEndpointClient {

    void connect() throws IOException;

    void disconnect() throws IOException;

}
