package ru.tsc.tambovtsev.tm.exception.field;


public final class RoleEmptyException extends AbstractFieldException {

    public RoleEmptyException() {
        super("Error! Role is empty...");
    }

}
